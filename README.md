
![Logo](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/th5xamgrr6se0x5ro4g6.png)


# **Picallti  🚲**

Mobile App for **bicycle rental**, since they are the most ecological, motivating and easy way to move from one place to another. Our virtual market gives the possibility to the owner to rent out his vehicle, and to the applicant to easily find the one he wants to have for the period he wants.
## **Authors :**

- [@SafiMehdi](https://www.github.com/SafiMehdi)
- [@hervelinne](https://www.github.com/hervelinne)
- [@fad232](https://www.github.com/fad232)
- [@echsimo](https://www.github.com/echsimo)
- [@IlhamOukassou](https://www.github.com/IlhamOukassou)
- [@OuissalElkorri]()
- [@NisrineMofakir]()
- [@ZeroualiIbtissam]()
